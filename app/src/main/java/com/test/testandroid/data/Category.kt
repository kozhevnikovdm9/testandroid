package com.test.testandroid.data

data class Category(
    val categoryId: Int,
    val categoryName: String
)