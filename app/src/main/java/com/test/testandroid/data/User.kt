package com.test.testandroid.data


data class User(
    val login: String,
    val password: String,
    val token: Int
)